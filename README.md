# Install
1. clone this repository
2. mkdir build; cd build
3. cmake ..
4. make

This will create an executable called SP.

# Usage
- ./SP <process_type> <input_path> <output_path> <superpixel_count>
- examples:
    - ./SP image test.jpg test_out.jpg 50
    - ./SP video test.mp4 test_out.mp4 50
